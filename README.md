# Prueba Tecnica

Prueba técnica programador Python.

## Objetivo
Revisar conceptos de API's y transformación de data usando las herramientas básicas de Python.

## Procedimiento.

1. Desde http://dummy.restapiexample.com/api/v1/employees obtener todos los empleados.
2. Por cada uno de los empleados obtener los 5 con mejor salario. (consultar http://dummy.restapiexample.com/)  Dichos datos deben ser guardados en la base de datos de su elección (PostgreSQL, MySQL, MongoDB, SQlite)
3. Guardar toda la base de datos en un archivo .json
4. Al ejecutarse el programa, se debe imprimir el tiempo de ejecución total del programa.
5. La prueba debe entregarse en un repositorio de Git, deseable con instrucciones de ejecución.